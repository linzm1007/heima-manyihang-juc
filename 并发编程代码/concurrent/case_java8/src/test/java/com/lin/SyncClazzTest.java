package com.lin;

/**
 * @Author zimingl
 * @Date 2023/4/30 0:06
 * @Description: 字节码分析
 */
public class SyncClazzTest {
    static final Object lock = new Object();

    static int counter = 0;

    public static void main(String[] args) {
        synchronized (lock) {
            counter++;
        }
    }
}
