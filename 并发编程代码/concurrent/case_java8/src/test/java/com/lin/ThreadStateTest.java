package com.lin;

import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

/**
 * @Author zimingl
 * @Date 2023/4/30 12:01
 * @Description: TODO
 */
public class ThreadStateTest {
    static Object lock = new Object();

    @SneakyThrows
    public static void test() {
        Thread thread = new Thread(() -> {
            try {
                synchronized (lock) {
                    lock.wait();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
//            try {
//                TimeUnit.SECONDS.sleep(2);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
        });
        thread.start();
        TimeUnit.MILLISECONDS.sleep(100);
        System.out.println(thread.getState());
        synchronized (lock) {
            lock.notify();
        }
        System.out.println(thread.getState());
    }

    public static void main(String[] args) {
        test();
    }
}
