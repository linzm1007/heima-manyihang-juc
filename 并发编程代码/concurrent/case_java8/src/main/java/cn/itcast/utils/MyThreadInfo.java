package cn.itcast.utils;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

/**
 * @Author zimingl
 * @Date 2023/4/19 22:59
 * @Description: ThreadInfo
 */
public class MyThreadInfo {
    public static void getThreadInfo() {
        ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();
        java.lang.management.ThreadInfo[] threadInfo = threadBean.dumpAllThreads(false, false);
        for(java.lang.management.ThreadInfo info : threadInfo) {
            System.out.println(info.getThreadId() + "--" + info.getThreadName() + "--" + info.getThreadState().name());
        }
    }
}
